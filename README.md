# CRS-Drinfeld-521

This git repository contains the companion C++/NTL code for the paper *Hard
Homogeneous Spaces from the Class Field Theory* authored by Antoine Leudière
and [Pierre-Jean Spaenlehauer](https://members.loria.fr/PJSpaenlehauer/index.html).

There are two main compilable units: `check.cc` and
`bench_CRS_Drinfeld_521.cc`.

### `check.cc`

The first compilable unit is `check.cc`; compile with:
```
g++ check.cc -o check -lntl -g
```
  
This software constructs a random Drinfeld module over a small extension field
whose characteric polynomial of the Frobenius defines a imaginary hyperelliptic
function field.

It implements the group action described in the paper, and runs a few checks to
verify this is indeed a commutative group action. In particular, it constructs
two low-degree places `P1` `P2` on the hyperelliptic curves, and it ensures
that `[P1] * ([P2] * j) = [P2] * ([P1] * j)`.

Note that this program may sometimes not terminate. This is an expected
behavior: it can happen when there are no place of prescribed degree on the
hyperelliptic curve (this is expected to happen with non-negligible probability
only for small parameters). In this case, our procedure to generate a random
place of this given degree goes into an infinite loop.

### `bench_CRS_Drinfeld_521.cc`

The second compilable unit is `bench_CRS_Drinfeld_521.cc`; compile with:
```
g++ -o bench_CRS_Drinfeld_521 bench_CRS_Drinfeld_521.cc -lntl -O3 -DNDEBUG -fopenmp
```

This executable is designed to measure the practical efficiency of the group
action on the isomorphism class of Drinfeld modules described in Section 4.1 of
the associated paper. The underlying hyperelliptic curve is defined over GF(2)
and it has genus 260. The order of the Jacobian is twice a 257-bit prime
number.

This software generates 1000 places of degree 35 on the curve and then computes
the group action for all of these places. It uses parallelization
via OpenMP; it can use up to 16 cores (this can be increased by changing the
`BLOCKSIZE` for a higher value if needed).

## License

This project is licensed under the GNU General Public License v3.0 - see the
[COPYING](COPYING) file for details.
