// Copyright 2022, Antoine Leudière, Pierre-Jean Spaenlehauer
//
// This file is part of DrinCRS-521
//
// DrinCRS-521 is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// DrinCRS-521 is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// DrinCRS-521. If not, see <https://www.gnu.org/licenses/>.


#ifndef BLOCK_SIZE
#error BLOCK_SIZE should be defined
#endif

#ifndef DEG_EXT
#error DEG_EXT should be defined
#endif

#ifndef __DRINCRS__
#define __DRINCRS__

#include <NTL/GF2E.h>
#include <NTL/GF2XFactoring.h>
#include <NTL/GF2EXFactoring.h>
#include <NTL/GF2EX.h>
#include <sstream>
#include <utility> // for pair
#include <cassert>
#include <vector>
#include <ctime>

using namespace NTL;
using namespace std;

GF2E omega;

void init()
{
  GF2E::init(BuildSparseIrred_GF2X(DEG_EXT));
  GF2X::HexOutput = 1;
  cout << "Field characteristic: " << GF2E::modulus() << endl;
  istringstream is(string("[0 1]"));
  is >> omega;
}

void apply_frob(GF2EX& f)
{
  if (f == GF2EX(0)) return;
  for (size_t i = 0; i <= deg(f); ++i)
    SetCoeff(f, i, coeff(f, i)*coeff(f, i));
}

// computes phi_X * Q dans K{tau}
GF2EX phi_left_multiply(const GF2E& Delta, const GF2EX& Q)
{
  GF2EX res = omega*Q;
  GF2EX Q2 = Q;
  apply_frob(Q2);
  Q2 <<= 1;
  res += Q2;
  apply_frob(Q2);
  Q2 <<= 1;
  res += Delta * Q2;
  return res;
}

const GF2X& FrobeniusNorm() 
{
  return GF2E::modulus();
}

// computes (u(phi_X), v(phi_X))
pair<GF2EX, GF2EX> eval_uv(const GF2E& Delta,
                           const GF2X& u, 
                           const GF2X& v) 
{
  assert(deg(v) < deg(u));
  vector<GF2EX> vec_powers_phi;
  GF2EX tmp = GF2EX(1);
  for (size_t i = 0; i < deg(u); ++i)
  {
    tmp = phi_left_multiply(Delta, tmp);
    vec_powers_phi.push_back(tmp);
  }

  GF2EX eval_u = u[0]*GF2EX(1);
  for (size_t i = 0; i < deg(u); ++i)
    eval_u += u[i+1]*vec_powers_phi[i];
  GF2EX eval_v = v[0]*GF2EX(1);
  for (size_t i = 0; i < deg(v); ++i)
    eval_v += v[i+1]*vec_powers_phi[i];
  return pair<GF2EX, GF2EX>(eval_u, eval_v);
}

const GF2X& FrobeniusTrace(const GF2E& Delta) 
{
//  GF2EX tmp = GF2EX(1);
//  GF2EX res = GF2EX(0);
//  for (size_t i = 0; i <= DEG_EXT; ++i) 
//  {
//    res += coeff(GF2E::modulus(), i) * tmp;
//    tmp = phi_left_multiply(Delta, tmp);
//    trunc(tmp, tmp, DEG_EXT + 1);
//  }
//  return rep(coeff(res, DEG_EXT));
  pair<GF2EX, GF2EX> r = eval_uv(Delta, GF2E::modulus(), GF2X(1)); 
  return rep(coeff(r.first, DEG_EXT));
}

// computes the trace of a in GF(2)[X]/u, provided u is irreducible
GF2 trace(const GF2X& a, const GF2X& u) 
{
  GF2X tmp = (a % u);
  GF2X res = tmp;
  for (size_t i = 1; i < deg(u); ++i)
  {
    tmp  = ((tmp*tmp) % u);
    res += tmp;
  }
  assert (deg(res) == 0 || res == GF2X(0));
  return coeff(res, 0);
}

// Produces a random place of degree d on the hyperelliptic curve (it excludes
// places of order 2)
const pair<GF2X, GF2X> rand_place(const GF2X& frobnorm,
                                  const GF2X& frobtrace,
                                  size_t d)
{
  GF2X u, v, gcd, s, t, gamma;
  bool flag = false;
  do {
    u = BuildRandomIrred(BuildIrred_GF2X(d));
    if (frobtrace % u == GF2X(0)) {
      flag = true;
      continue; // excludes places of order 2
    }
    XGCD(gcd, s, t, frobtrace % u, u);
    assert (gcd == GF2(1));
    gamma = (s * s * frobnorm) % u;
  } while (trace(gamma, u) != GF2(0) || flag);
  v = GF2X(0);

  assert( d % 2 == 1 ); // at the moment, it only works for odd d
  GF2X gamma2 = (gamma * gamma) %u;
  for (size_t i = 0; i < (deg(u)-1)/2; ++i)
  {
    v += gamma2;
    gamma2 = (gamma2 * gamma2) % u;
    gamma2 = (gamma2 * gamma2) % u;
  }
  assert ( (v * v + v + gamma) % u == GF2X(0) );

  v = (v * frobtrace) % u;
  assert ((v * v + frobtrace * v + frobnorm) % u == GF2X(0));
  return pair<GF2X, GF2X>(u, v);
}


// computes the remainder in the right divison of tau^DEG_EXT by u(phi)
GF2EX tauK_rem_phi(const GF2EX& u_phi)
{
  GF2EX u_phi2 = u_phi/LeadCoeff(u_phi);
  GF2EX vec_reds[BLOCK_SIZE];
  vec_reds[0] = (GF2EX(1) << deg(u_phi2)) - u_phi2;

  GF2EX table[BLOCK_SIZE];
  GF2EX res = vec_reds[0];
  for (size_t j = 1; j < BLOCK_SIZE; ++j) 
  {
    apply_frob(res);
    res <<= 1;
    res -= res[deg(u_phi2)] * vec_reds[0];
    trunc(res, res, deg(u_phi2));
    vec_reds[j] = res;
  }
 
  for (size_t i = 0; i < (DEG_EXT - deg(u_phi2) - BLOCK_SIZE + 1)/BLOCK_SIZE; ++i)
  {
    for (size_t j = 0; j < BLOCK_SIZE; ++j)
      apply_frob(res);
    res <<= BLOCK_SIZE;

#pragma omp parallel for
    for (size_t j = 0; j < BLOCK_SIZE; ++j) 
    {
      GF2E::init(BuildSparseIrred_GF2X(DEG_EXT));
      table[j] = res[deg(u_phi2) + j] * vec_reds[j];
    }
    for (size_t j = 0; j < BLOCK_SIZE; ++j)
      res -= table[j];
    trunc(res, res, deg(u_phi2));
  }
  for (size_t i = 0; i < (DEG_EXT - deg(u_phi2) - BLOCK_SIZE + 1) % BLOCK_SIZE; ++i)
  {
    apply_frob(res);
    res <<= 1;
    res -= res[deg(u_phi2)]*u_phi2;
  }
  return res;
}

// computes the remainder in the right division of a by b
void right_rem(GF2EX& a, const GF2EX& b)
{
  if (deg(a) < deg(b))
    return;
  GF2EX b2 = b/LeadCoeff(b);
  for (size_t i = 0; i < deg(a) - deg(b); ++i)
    apply_frob(b2);
  b2 <<= (deg(a) - deg(b));
  a -= LeadCoeff(a)*b2;
  right_rem(a, b);
}


// computes RGCD(u(phi_X), tauK - v(phi_X))
GF2EX isog_RGCD(const pair<GF2EX, GF2EX>& uv_phi)
{
//  std::clock_t c_start = std::clock();

  GF2EX w2 = tauK_rem_phi(uv_phi.first);
//  GF2EX w2 = GF2EX(1) << DEG_EXT;
  
//  std::clock_t c_end = std::clock();
//  double time_elapsed_ms = 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC;
//  cout << "CPU time for computing tauK mod u(phiX): " << time_elapsed_ms << " ms" << endl;
//  cout << deg(w2) << endl;

  assert(deg(w2) >= 0);
   
  w2 -= uv_phi.second;
  GF2EX w1 = uv_phi.first;

//  c_start = std::clock();
  // Euclide's algo
  while (w2 != GF2EX(0))
  {
    right_rem(w1, w2);
    swap(w1, w2);
  }
//  c_end = std::clock();
//  time_elapsed_ms = 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC;
//  cout << "CPU time for computing the RGCD: " << time_elapsed_ms << " ms" << endl;
  
  w1 /= LeadCoeff(w1);
  return w1;
}

GF2E GroupAction(const GF2E& j, const GF2X& u, const GF2X& v)
{
//  std::clock_t c_start = std::clock();
//  cout << j << endl;
  pair<GF2EX, GF2EX> uv_phi = eval_uv(1/j, u, v);
//  std::clock_t c_end = std::clock();

//  double time_elapsed_ms = 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC;
//  cout << "CPU time for computing (u(phiX), v(phiX)): " << time_elapsed_ms << " ms" << endl;

  GF2EX w = isog_RGCD(uv_phi);
  GF2E g2     = 1/(w[0]*w[0])*(w[0]+w[1]*(omega*omega - omega));
  GF2E Delta2 = 1/j;
  for (size_t i = 0; i < deg(w); ++i)
    Delta2 = Delta2*Delta2;

//  GF2E w03 = w[0]*w[0]*w[0];
//  GF2E omega_a = omega*omega - omega;
//  GF2E omega_a3 = omega_a*omega_a*omega_a;
//  return j/w03 * (1+w[1]*w[1]*w[1]/w03*omega_a3);
  return g2*g2*g2/Delta2;
}

// Arithmetic in the Picard group
pair<GF2X, GF2X> jac_law(const GF2X& frob_norm, 
                         const GF2X& frob_trace, 
                         const GF2X& u1, 
                         const GF2X& v1, 
                         const GF2X& u2, 
                         const GF2X& v2)
{
  size_t genus = deg(frob_norm)/2; 
  assert ( (v1*v1 + frob_trace*v1 - frob_norm) % u1 == GF2X(0) );
  assert ( (v2*v2 + frob_trace*v2 - frob_norm) % u2 == GF2X(0) );
  GF2X d1, e1, e2, d, c1, c2;  

  XGCD(d1, e1, e2, u1, u2);
  XGCD(d, c1, c2, d1, v1+v2+frob_trace);
  GF2X s1 = c1*e1; 
  GF2X s2 = c1*e2; 
  GF2X s3 = c2;
  GF2X u = (u1*u2)/(d*d); 
  GF2X v = (s1*u1*v2+s2*u2*v1+s3*(v1*v2+frob_norm))/d; 
  v = v % u; 
  do {
    GF2X uu = (frob_norm-v*frob_trace-v*v)/u; 
    GF2X vv = (-frob_trace-v) % uu; 
    u = uu; 
    v = vv; 
  } while(deg(u) > genus);
  assert ( (v*v + frob_trace*v + frob_norm) % u == GF2X(0) );
  return pair<GF2X, GF2X>(u,v);
}
#endif
