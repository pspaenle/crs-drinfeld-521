// g++ -o bench_CRS_Drinfeld_521 bench_CRS_Drinfeld_521.cc -lntl -O3 -DNDEBUG -fopenmp

// Copyright 2022, Antoine Leudière, Pierre-Jean Spaenlehauer
//
// This file is part of DrinCRS-521
//
// DrinCRS-521 is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// DrinCRS-521 is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// DrinCRS-521. If not, see <https://www.gnu.org/licenses/>.


#define BLOCK_SIZE 16
#define DEG_EXT 521

#include "DrinCRS.h"

int main()
{
  cerr << "Polynomials in GF(2)[X] are given in hexadecimal NTL notation" << endl;
  init();

  GF2E Delta, j;
  istringstream(string("0xb985b4ce23bd9cf992f1176e17c27dab7ae6727013112a2804cb64abccc7cce061e12786bb3248809922da35d3b624d67d08087e07c260fcaa9807a420ca83fa95")) >> j;
  Delta = 1/j;
  GF2X ftrace, fnorm;

  fnorm = GF2E::modulus();
  istringstream(string("0xb1ffea4ab7e58b96adf4e4972d7db9184821c1d64b375df52669c60973bb80dee")) >> ftrace;

  cerr << "j-invariant of the Drinfeld module:" << endl;
  cerr << "j := \"" << 1/Delta << "\";" << endl << endl;
  cerr << "Coefficients of the characteristic polynomial of the Frobenius endomorphism:" << endl;
  cerr << "ftrace := \"" << ftrace << "\";" << endl;
  cerr << "fnorm := \"" << fnorm << "\";" << endl << endl;
  long deg_u = 35;

  pair<GF2X, GF2X> uv = rand_place(fnorm, ftrace, deg_u);

  // Checks that the characteristic polynomial vanishes at the Frobenius
  // endomorphism
  assert ( (uv.second*uv.second + ftrace*uv.second + fnorm) % uv.first == 0 );
  assert ( (uv2.second*uv2.second + ftrace*uv2.second + fnorm) % uv2.first == 0 );

  cout << "starting benchmark (computes 1000 times the group action with respect to places of degree 35 on the underlying hyperelliptic curve)" << endl;

  GF2E res = j;
  for (size_t i = 0; i < 1000; ++i)
  {
    // Computes a random place of degree 35 on the hyperelliptic curve
    uv = rand_place(fnorm, ftrace, deg_u);
    // Computes the group action
    res = GroupAction(res, uv.first, uv.second);
  }

  return 0;
}
