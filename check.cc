// g++ check.cc -o check -lntl -g

// Copyright 2022, Antoine Leudière, Pierre-Jean Spaenlehauer
//
// This file is part of DrinCRS-521
//
// DrinCRS-521 is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// DrinCRS-521 is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// DrinCRS-521. If not, see <https://www.gnu.org/licenses/>.


#define BLOCK_SIZE 1
#define DEG_EXT 13

#include "DrinCRS.h"

int main()
{
  cerr << "Polynomials in GF(2)[X] are given in hexadecimal NTL notation" << endl;
  init();
  GF2X::HexOutput = 0;
  cout << GF2E::modulus() << endl;
  
  GF2E Delta, j;
  GF2X gcd, s, t;
  GF2X ftrace;
  GF2X fnorm;
  do {
    do {
      Delta = random_GF2E();
    } while (Delta == GF2E(0));
    // Computing the trace of the Frobenius endomorphism
    ftrace = FrobeniusTrace(Delta);
    fnorm  = FrobeniusNorm();
    // check nonsingularity, so that the characteristic polynomial of the
    // Frobenius endomorphism defines a imaginary hyperelliptic curve
    XGCD(gcd, s, t, diff(ftrace), ftrace);
  } while (ftrace == GF2X(0) ||
           gcd != GF2X(1) || 
           GCD(ftrace, (diff(fnorm)*s)*(diff(fnorm)*s) + fnorm) != GF2X(1)
           || !IterIrredTest(ftrace)
           );

//  Y^2+h(X)Y+f(X) = 0
//  h(X) = 0
//  h'(X)Y+f'(X) = 0
  j = 1/Delta;
  ftrace = FrobeniusTrace(Delta);
  fnorm  = FrobeniusNorm();

  GF2EX phiX = omega + GF2EX(INIT_MONO, 1, 1) + GF2EX(INIT_MONO, 2, Delta);
  GF2EX fnorm_phiX = GF2EX(0);
  GF2EX powphiX = GF2EX(1);
  for (size_t i = 0; i <= deg(fnorm); ++i) 
  {
    if(coeff(fnorm, i) == GF2(1)) 
      fnorm_phiX += powphiX;
    powphiX = phi_left_multiply(Delta, powphiX);
  }
  GF2EX ftrace_phiX = GF2EX(0);
  powphiX = GF2EX(1);
  for (size_t i = 0; i <= deg(ftrace); ++i) 
  {
    if(coeff(ftrace, i) == GF2(1)) 
      ftrace_phiX += powphiX;
    powphiX = phi_left_multiply(Delta, powphiX);
  }

  GF2EX evalpolchar = GF2EX(INIT_MONO, 2*DEG_EXT, 1) + (ftrace_phiX << DEG_EXT) + fnorm_phiX;
  assert(evalpolchar == GF2EX(0));
  cerr << "j-invariant of the Drinfeld module:" << endl;
  cerr << "j := \"" << 1/Delta << "\";" << endl << endl;
  cerr << "Coefficients of the characteristic polynomial of the Frobenius endomorphism:" << endl;
  cerr << "ftrace := \"" << ftrace << "\";" << endl;
  cerr << "fnorm := \"" << fnorm << "\";" << endl << endl;
  long deg_u = 5;

  pair<GF2X, GF2X> uv1 = rand_place(fnorm, ftrace, deg_u);
  pair<GF2X, GF2X> uv2 = rand_place(fnorm, ftrace, deg_u);

//  cout << uv1.first << " " << uv1.second << endl;
//  cout << uv2.first << " " << uv2.second << endl << endl;

  cerr << "Computation of the group action: " << endl;

  GF2E j1 = GroupAction(j, uv1.first, uv1.second);
  cerr << "([P1] * j): " << j1 << endl;
  GF2E j2 = GroupAction(j, uv2.first, uv2.second);
  cerr << "([P2] * j): " << j2 << endl;

  GF2E j3 = GroupAction(j2, uv1.first, uv1.second);
  cerr << "[P1] * ([P2] * j): " << j3 << endl;
  GF2E j4 = GroupAction(j1, uv2.first, uv2.second);
  cerr << "[P2] * ([P1] * j): " << j4 << endl;

  assert (j3 == j4);

  return 0;
}

